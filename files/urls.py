from django.conf.urls import url

from files import views

urlpatterns = [
    url(r'^$', views.ListFilesView.as_view(), name='list_files'),
    url(r'^create_file/$', views.CreateFileView.as_view(), name='create_file'),
    url(r'^delete_file/(?P<pk>\d+)$',
        views.DeleteFileView.as_view(),
        name='delete_file'),
]
