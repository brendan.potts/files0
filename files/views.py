from django.core.urlresolvers import reverse_lazy
from django.views.generic.edit import CreateView, DeleteView
from django.views.generic.list import ListView

from berou.mixins import OwnershipRequiredMixin, LoginRequiredMixin
from files import models


class ListFilesView(ListView):
    model = models.File
    template_name = 'files/list_files.html'

    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user).all()

    def get_context_data(self, **kwargs):
        context = super(ListFilesView, self).get_context_data(**kwargs)
        create_view = CreateFileView()
        create_view.request = self.request
        context['form'] = create_view.get_form()
        return context


class CreateFileView(LoginRequiredMixin, CreateView):
    model = models.File
    template_name = 'files/create_file.html'
    fields = ['file']

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.name = form.instance.file.name
        return super(CreateFileView, self).form_valid(form)


class DeleteFileView(LoginRequiredMixin, OwnershipRequiredMixin, DeleteView):
    model = models.File
    template_name = 'files/confirm_delete_file.html'
    success_url = reverse_lazy('files:list_files')
