import os

from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models


class File(models.Model):
    name = models.CharField(max_length=200)
    file = models.FileField(upload_to=(
        lambda self, filename: os.path.join(self.user.username, filename)
    ))
    user = models.ForeignKey(settings.AUTH_USER_MODEL, default=None)

    def get_absolute_url(self):
        return reverse('files:list_files')

    def __str__(self):
        string_length = 20
        if len(self.name) > string_length:
            name = self.name[:string_length-1] + '...'
        else:
            name = self.name
        return u'<File: name="{name}">'.format(name=name)

    def save(self, *args, **kwargs):
        folder = os.path.join(settings.MEDIA_ROOT, self.user.username)
        if not os.path.isdir(folder):
            os.mkdir(folder)
        return super(File, self).save(*args, **kwargs)
